# Bibliothèque d'internationalisation

Ce plugin intègre la librairie Intl de Commerce Guys : https://github.com/commerceguys/intl/ ainsi que quelques fonctions facilitant leur utilisant dans SPIP.

Cette librairie contient :
- la liste des devises et leurs infos détaillées et nom dans plein de langues
- la liste des comportements régionaux ("locales")
- le formatage des nombres suivant les régions
- le formatage des montants (càd de nombre + une devise) suivant les régions

Des saisies et fonctions d'affichage sont ajoutées par-dessus afin d'utiliser tout ça facilement dans SPIP.
