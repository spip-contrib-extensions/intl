<?php

/*
 * Paiement Bancaire
 * module de paiement bancaire multi prestataires
 * stockage des transactions
 *
 * Auteurs :
 * Cedric Morin, Nursit.com
 * (c) 2012-2019 - Distribue sous licence GNU/GPL
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Upgrade de la base
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 */
function intl_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];
	$maj['create'] = [
		['intl_migrer_de_prix'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function intl_migrer_de_prix() {
	include_spip('inc/config');

	// Si on trouve la config de prix, que le plugin soit actif ou pas
	if ($config_prix = lire_config('prix')) {
		if (isset($config_prix['devise_defaut'])) {
			ecrire_config('intl/devise_defaut', $config_prix['devise_defaut']);
		}

		if (isset($config_prix['locales'])) {
			ecrire_config('intl/locales', $config_prix['locales']);
		}
	}
}

/**
 * Desinstallation
 *
 * @param string $nom_meta_base_version
 */
function intl_vider_tables($nom_meta_base_version) {
	effacer_meta($nom_meta_base_version);
	effacer_meta('intl');
}
