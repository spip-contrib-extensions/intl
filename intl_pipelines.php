<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function intl_bank_lister_devises($devises) {
	include_spip('intl_fonctions');

	return intl_lister_devises();
}

function intl_bank_devise_defaut($devise) {
	include_spip('intl_fonctions');

	// On va chercher la devise par défaut configurée
	return intl_devise_defaut();
}
