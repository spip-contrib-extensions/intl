<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_choisir_option' => 'Please choose an option',
	'cfg_devise_label' => 'Default currency',
	'cfg_locale_non' => 'No local option',
	'cfg_locales_legend' => 'Mapping between SPIP languages and regional ',
	'cfg_message_extensions_php_ou' => 'One of the following PHP extensions is required: <em>@ext@</em>. <br> Install one or contact your host (preferably the first listed).', # RELIRE
	'cfg_titre' => 'Configure local behaviors',

	// I
	'icone_voir_demo' => 'Demo page',
	'icone_voir_devises' => 'View currencies',
	'info_1_devises' => '1 currency',
	'info_nb_devises' => '@nb@ currencies',

	// L
	'label_devise_code' => 'Alphabetical code',
	'label_devise_code_num' => 'Numeric code',
	'label_devise_fraction' => 'Fraction',
	'label_devise_nom' => 'Name',
	'label_devise_symbole' => 'Symbol',

	// S
	'saisie_devise_description' => 'Single or multiple currency selection',
	'saisie_devise_option_code_alpha_label' => '3-letter alphabetical code',
	'saisie_devise_option_code_label' => 'Value used',
	'saisie_devise_option_code_num_label' => 'Numeric code',
	'saisie_devise_option_multiple_label' => 'Allow to select multiple currencies',
	'saisie_devise_titre' => 'Currency',

	// T
	'titre_devises' => 'Currencies',
);
