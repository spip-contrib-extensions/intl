<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_choisir_option' => 'Por favor elige una opción', # RELIRE
	'cfg_devise_label' => 'Moneda por defecto', # RELIRE

	// I
	'icone_voir_demo' => 'Página de demostración', # RELIRE
	'icone_voir_devises' => 'Ver monedas', # RELIRE
	'info_1_devises' => '1 moneda', # RELIRE
	'info_nb_devises' => '@nb@ monedas', # RELIRE

	// L
	'label_devise_code' => 'Código alfabético', # RELIRE
	'label_devise_code_num' => 'Código numérico', # RELIRE
	'label_devise_fraction' => 'Fracción', # RELIRE
	'label_devise_nom' => 'apellido', # RELIRE
	'label_devise_symbole' => 'Símbolo', # RELIRE

	// S
	'saisie_devise_option_code_num_label' => 'Código numérico', # RELIRE

	// T
	'titre_devises' => 'Monedas', # RELIRE
);
