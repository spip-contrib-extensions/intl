<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/prix.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_choisir_option' => 'Veuillez choisir une option',
	'cfg_currency_display_label' => 'Affichage par défaut',
	'cfg_currency_display_explication' => 'Toutes les devises n’ont pas de symbole, dans ce cas le code sera utilisé.',
	'cfg_currency_display_code' => 'Utiliser le code',
	'cfg_currency_display_symbol' => 'Utiliser le symbole',
	'cfg_devise_label' => 'Devise par défaut',
	'cfg_devises_legend' => 'Devises',
	'cfg_locale_non' => 'Pas d’option régionale',
	'cfg_locales_explication' => 'Pour chaque langue du site, il peut y avoir plusieurs variantes régionales. Vous pouvez ici dire à quelle variante officielle correspond chaque langue choisie pour votre site.',
	'cfg_locales_legend' => 'Correspondance entre les langues du site et les variantes régionales officielles',
	'cfg_message_extensions_php_ou' => 'Une des extensions PHP suivante est nécessaire : <em>@ext@</em>. <br>Installez-en une ou contactez votre hébergeur (de préférence la 1ère listée).',
	'cfg_titre' => 'Configurer les comportements régionaux',

	// I
	'icone_voir_devises' => 'Voir les devises',
	'info_1_devises' => '1 devise',
	'info_nb_devises' => '@nb@ devises',

	// L
	'label_devise_code' => 'Code alphabétique',
	'label_devise_code_num' => 'Code numérique',
	'label_devise_fraction' => 'Fraction',
	'label_devise_nom' => 'Nom',
	'label_devise_symbole' => 'Symbole',

	// S
	'saisie_devise_description' => 'Sélection unique ou multiple de devises',
	'saisie_devise_option_code_alpha_label' => 'Code alphabétique à 3 lettres',
	'saisie_devise_option_code_label' => 'Valeur utilisée',
	'saisie_devise_option_code_num_label' => 'Code numérique',
	'saisie_devise_option_multiple_label' => 'Permettre de sélectionner plusieurs devises',
	'saisie_devise_titre' => 'Devise',

	// T
	'titre_devises' => 'Devises',
);
