<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/prix?lang_cible=ja
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cfg_choisir_option' => 'オプションを選択してください',
	'cfg_devise_label' => 'デフォールトの通貨',
	'cfg_locale_non' => '地域オプションがありません',

	// I
	'icone_voir_demo' => 'デモページ',
	'icone_voir_devises' => '通貨を参照',
	'info_1_devises' => '1 通貨',
	'info_nb_devises' => '@nb@ 通貨',

	// L
	'label_devise_code' => 'アルファベット・コード',
	'label_devise_code_num' => '数字コード',
	'label_devise_fraction' => '小数',
	'label_devise_nom' => '名前',
	'label_devise_symbole' => 'シンボル',

	// S
	'saisie_devise_description' => '1つあるいは複数の通貨の選択',
	'saisie_devise_option_code_alpha_label' => '3文字のアルファベット・コード',
	'saisie_devise_option_code_label' => '使われる価格',
	'saisie_devise_option_code_num_label' => '数字コード',
	'saisie_devise_option_multiple_label' => '複数の通貨の選択を許可する',
	'saisie_devise_titre' => '通貨',

	// T
	'titre_devises' => '通貨',
);
