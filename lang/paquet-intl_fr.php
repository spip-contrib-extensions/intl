<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/intl.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'intl_description' => 'Des outils pour manipuler les devises, les comportements régionaux sur les nombres et les montants. Ce plugin intègre la bibliothèque Intl de Commerce Guys et fournit des saisies et des fonctions autour, facilitant leur utilisation dans SPIP.',
	'intl_nom' => 'Bibliothèque d’internationalisation',
	'intl_slogan' => 'Nombres et montants, selon les devises et régions.'
);
