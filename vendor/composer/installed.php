<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '8d9b3e4d058c61d1353a1da80663b3103c36d952',
        'name' => 'spip-contrib-extensions/intl',
        'dev' => true,
    ),
    'versions' => array(
        'commerceguys/intl' => array(
            'pretty_version' => 'v1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../commerceguys/intl',
            'aliases' => array(),
            'reference' => 'cab3b55dbf8c1753fe54457404082c777a8c154f',
            'dev_requirement' => false,
        ),
        'spip-contrib-extensions/intl' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '8d9b3e4d058c61d1353a1da80663b3103c36d952',
            'dev_requirement' => false,
        ),
    ),
);
